import json
import logging
import sqlite3
from sseclient import SSEClient as EventSource

class FibonacciService:
    @classmethod
    def calculate(cls, number:int=0)->int:
        #TODO: calculate this!
        return 0

class DbWriter:
    def __init__(self):
        try:
            self.__db =  "wikievents.db"
            con = sqlite3.connect(self.__db)
            cursorObj = con.cursor()
            cursorObj.execute("""CREATE TABLE IF NOT EXISTS wiki_events(id integer PRIMARY KEY AUTOINCREMENT
                , username text, change text, fibonacci text)""")
            con.commit()
        except Exception as e:
            logging.critical(f'Ups we can\'t connect to {self.__db}')
            raise e
        
    def write(self, username, event, fibonacci):
        try:
            con = sqlite3.connect(self.__db)
            cursor = con.cursor()
            cursor.execute(
                f"INSERT INTO wiki_events (username, change, fibonacci) VALUES ('{username}','{event}','{fibonacci}')")
            con.commit()
        except Exception as e:
            logging.critical(f'Ups we can\'t write to {self.__db}, {event}')
            raise e
        finally:
            con.close()

writer = DbWriter()
url = 'https://stream.wikimedia.org/v2/stream/recentchange'
for event in EventSource(url):
    if event.event == 'message':
        try:
            change = json.loads(event.data)
        except ValueError:
            pass
        else:
            try:
                #Why this must be calculated? dunno, product team requirement
                fib = FibonacciService.calculate(f'{id}'.format(**change))
                print('{user} edited {title}'.format(**change))
            except KeyError:
                fib = -1
                pass
            writer.write(change['user'],json.dumps(change).replace("'","\\u0027"), fib)
            