# Wiki stream CQRS

We want you to **lead** the conversation about how to tackle this trivial problem: Some existing code needs to be refactored and you are in charge to do it in a short period of time. Please remember that _good **on time** is better than perfect **never**_ and complete as many tasks that you can in 90 minutes. You are free to use any tool that you want, we will need you to share your computer's screen all the time and will throw some questions about your decisions to better understand why did you choose X instead of Y.

## Context

The service consumes an [event stream](https://wikitech.wikimedia.org/wiki/Event_Platform/EventStreams) from the Wikipedia foundation. It uses Server Side Events as protocol, and the client saves the data from each event along with a custom calculation (Fibonacci series) locally. Data to be consumed are exposed via a single endpoint (/events/_username_) returning all events for a given user. The view was written using the Cherrypy framework.

## Requirements
- Install python3
- Install virtualenv
- Install pip
- Install requirements `pip install -r requirements.txt`

## Running the Command

`python consumer.py`

## Running the Query

`python main.py`

sample request `http://127.0.0.1:8080/?user_name=Einstein2`

## Tasks
- Read the code
- Create a simple diagram of the current architecture
- Proposed improvements to the way either the service or the listener handle errors in the data
- Implement the FibonacciService's _calculate_ method
- Create a minimal set of tests (using any framework you choose)
- Create a merge request of your changes
- Propose a more suitable architecture for the CQRS implementation
