import json
import logging
import sqlite3
import cherrypy

class DbWriter:
    def __init__(self):
        self.__db =  "wikievents.db"
        
    def read(self, username):
        values = []
        try:
            con = sqlite3.connect(self.__db)
            cursor = con.cursor()
            cursor.execute(
                f"SELECT * FROM wiki_events WHERE username = '{username}'")
            output = cursor.fetchall()
            for row in output:
                values.append(row)
            con.commit()
        except Exception as e:
            logging.critical(f'Ups we can\'t read from {self.__db}')
            raise e
        finally:
            con.close()
        return values

class EventAPI(object):
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def index(self, user_name):
        db = DbWriter()
        results = db.read(user_name)
        return {"user_name": user_name, "total":len(results),"results":results}

cherrypy.quickstart(EventAPI())